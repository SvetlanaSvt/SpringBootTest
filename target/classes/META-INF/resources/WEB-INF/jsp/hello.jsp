<%--
  Created by IntelliJ IDEA.
  User: Svetik
  Date: 13.12.2016
  Time: 18:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
</head>
<body>
<div>
    <p><b>Save user from jsp</b></p>
</div>
<div>
<form name="registrationForm" method="post" action="${pageContext.servletContext.contextPath}/user">
    <table>
        <tr>
            <td>Логин:</td>
            <td><input type="text" name="username" required>*</td>
        </tr>
        <tr>
            <td>Пароль:</td>
            <td><input type="password" name="password" required>*</td>
        </tr>
        <tr>
            <td>Подтвердите пароль:</td>
            <td><input type="password" name="confirmPassword" required>*</td>
        </tr>
        <tr>
            <td>Имя:</td>
            <td><input type="text" name="firstName"></td>
        </tr>
        <tr>
            <td>Фамилия:</td>
            <td><input type="text" name="lastName"></td>
        </tr>
        <tr>
            <td>email:</td>
            <td><input type="email" name="email" required>*</td>
        </tr>
        <tr>
            <td>Пол:</td>
            <td><select type="text" name="sex">
                <option value="Мужской">Мужской</option>
                <option value="Женский">Женский</option>
            </select>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="2"><input type="submit" value="Зарегистрироваться" name="reg"></td>
        </tr>
    </table>
    <tr>
        * обязательные поля для заполнения
    </tr>
</form>
</div>
<div>
    <h4>To view the documentation for RESTful API, click on the link
        <b><a href="jsondoc-ui.html">JSONDOC</a></b>
        and enter the address http://localhost:8080/jsondoc
    </h4>
</div>
</body>
</html>
