package svt.controller;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import svt.model.Role;
import svt.model.User;
import svt.repositories.RoleRepository;
import svt.repositories.UserRepository;

@Controller
@Api(name = "Cinema JSP", description = "Provide a list of methods in a cinema JSP")
public class DemoController {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @RequestMapping("/")
    @ApiMethod(description = "Start from JSP. UserForm and link to JSONDOC")
    public ModelAndView hello() {
        return new ModelAndView("hello", "user", new User());
    }

    @RequestMapping(value = "/user", method = RequestMethod.POST)
    @ApiMethod(description = "Save user. This method works from JSP")
    public ModelAndView saveMovie(@ModelAttribute("user") User user) {
        Role role = roleRepository.getOne(1L);
        user.setRole(role);
        userRepository.save(user);
        return new ModelAndView("user", "user", user);
    }
}
