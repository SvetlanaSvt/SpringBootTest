package svt.controller;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import svt.model.*;
import svt.repositories.*;

import java.util.List;

@RestController
@Api(name = "Cinema Api", description = "Provide a list of methods in a cinema",
        stage = ApiStage.RC)
public class DemoRestController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    MovieRepository movieRepository;
    @Autowired
    GenreRepository genreRepository;
    @Autowired
    HallRepository hallRepository;
    @Autowired
    RowsRepository rowsRepository;
    @Autowired
    SessionRepository sessionRepository;
    @Autowired
    TicketsRepository ticketsRepository;

    @RequestMapping("/role")
    @ApiMethod(description = "Get all roles from database")
    public List<Role> getAllRole() {
        return roleRepository.findAll();
    }

    @RequestMapping("/user")
    @ApiMethod(description = "Get all users from database")
    public List<User> getAllUser() {
        return userRepository.findAll();
    }

    @RequestMapping("/genre")
    @ApiMethod(description = "Get all genres from database")
    public List<Genre> getAllGenre() {
        return genreRepository.findAll();
    }

    @RequestMapping("/movie")
    @ApiMethod(description = "Get all movies from database")
    public List<Movie> getAllMovie() {
        return movieRepository.findAll();
    }

    @RequestMapping(value = "/movie/{id}", method = RequestMethod.GET)
    @ApiMethod(description = "Get one movie by id")
    public Movie getMovieById(@ApiPathParam(name = "id") @PathVariable long id) {
        return movieRepository.findOne(id);
    }

    @RequestMapping(value = "/movie/search/{title}")
    @ApiMethod(description = "Find movie by title")
    public List<Movie> getMovieByTitle(@ApiPathParam(name = "title") @PathVariable String title) {
        return movieRepository.findByTitle(title);
    }

    @RequestMapping(value = "/movie/create", method = RequestMethod.POST)
    @ApiMethod(description = "Save new movie into database. The rentEnd and rentStart should enter in this format \"yyyy-MM-DD\" without braces.")
    public List<Movie> createMovie(@RequestBody Movie movie) {
        movieRepository.save(movie);
        return movieRepository.findAll();
    }

    @RequestMapping(value = "/movie/delete/{id}")
    @ApiMethod(description = "Delete movie by id")
    public List<Movie> deleteMovie(@ApiPathParam(name = "id") @PathVariable long id) {
        movieRepository.delete(id);
        return movieRepository.findAll();
    }

    @RequestMapping("/hall")
    @ApiMethod(description = "Get all halls in Cinema from database")
    public List<Hall> getAllHall() {
        return hallRepository.findAll();
    }

    //Присвоение Id местам в зале в таблице rows в базе данных ЕДИНОРАЗОВО, чтоб не прописывать вручную
    @RequestMapping("/hall/save")
    @ApiMethod(description = "Assigning Id seats in the hall in table \"rows\" in the database FOR ONE TIME," +
                            " so as not to register manually")
    public List<Rows> saveRows() {
        List<Hall> halls = hallRepository.findAll();
        for (Hall hall : halls) {
            int row = hall.getRow();
            int seat = hall.getSeat();
            for (int j = 1; j <= row; j++) {
                for (int k = 1; k <= seat; k++) {
                    Rows r = new Rows();
                    r.setHall(hall);
                    r.setRowNumber(j);
                    r.setSeatNumber(k);
                    rowsRepository.save(r);
                }
            }
        }
        return rowsRepository.findAll();
    }
}
