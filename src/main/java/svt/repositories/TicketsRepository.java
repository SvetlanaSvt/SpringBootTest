package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Tickets;

@Repository
public interface TicketsRepository extends JpaRepository<Tickets, Long> {
}
