package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {
}
