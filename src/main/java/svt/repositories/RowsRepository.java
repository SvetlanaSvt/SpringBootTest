package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Rows;

@Repository
public interface RowsRepository extends JpaRepository<Rows, Long> {
}
