package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
}
