package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Session;

@Repository
public interface SessionRepository extends JpaRepository<Session, Long> {
}
