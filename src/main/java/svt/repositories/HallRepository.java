package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Hall;

@Repository
public interface HallRepository extends JpaRepository<Hall, Long> {
}
