package svt.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import svt.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
}
